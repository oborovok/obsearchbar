#
#  Be sure to run `pod spec lint OBSearchBar.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "OBSearchBar"
  s.version      = "0.0.2"
  s.summary      = "It is a search bar with dynamic list"
  s.description  = <<-DESC
"It is a search bar with dynamic list"
                   DESC

  s.homepage     = "https://bitbucket.org/oborovok/obsearchbar"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "Oleksandr Borovok" => "borovok@online.ua" }

  s.platform     = :ios, "9.0"
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.0' }

  s.source       = { :git => "https://bitbucket.org/oborovok/obsearchbar.git", :tag => "#{s.version}" }

  s.source_files  = "OBSearchBar/Classes/**/*"

  # s.public_header_files = "OBSearchBar/Classes/**/*.h"

  s.resources = "Resources/*.png"
  s.resource_bundles = {
    'OBSearchBar' => ['OBSearchBar/Assets/*.png']
  }
  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"

end
