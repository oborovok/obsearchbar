//
//  OBSearchBar.swift
//  testSearchBar
//
//  Created by Oleksandr Borovok on 11/15/17.
//  Copyright © 2017 Oleksandr Borovok. All rights reserved.
//

import UIKit

protocol OBSearchBarDelegate {
    func searchBar(_ searchBar: OBSearchBar, resized height: CGFloat)
    func searchBar(_ searchBar: OBSearchBar, textInputed text: String?)
    func searchBar(_ searchBar: OBSearchBar, textEditEnded text: String?)
    func searchBarKeyboardWasClosed(_ searchBar: OBSearchBar)
}

@IBDesignable class OBSearchBar: UIView {

    // MARK: - Properties Constants
    private let searchBarNibName = "OBSearchBar"
    
    fileprivate let reusableCellIdentifier = "OBSearchBarReusableCellIdentifier"
    fileprivate let heightForRow = CGFloat(30)
    
    fileprivate let heightViewMin = CGFloat(55)
    fileprivate let heightViewMiddle = CGFloat(85)
    fileprivate let heightViewMax = CGFloat(155)
    
    // MARK: - Properties Inspectable
    @IBInspectable var searchBarColor: UIColor! {
        didSet {
            layer.backgroundColor = searchBarColor.cgColor
            view.backgroundColor = searchBarColor
        }
    }
    
    // MARK: - Properties Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties Public
    var delegate: OBSearchBarDelegate?
    
    var view: UIView!
    
    var commonList = [String]()
    
    // MARK: - Properties Private
    fileprivate var filter: String?
    fileprivate var hasButtonDeleteStatus = false {
        didSet {
            rightButton.isSelected = hasButtonDeleteStatus
            
            if (hasButtonDeleteStatus) {
                searchTextField.text = ""
                filter = ""
                tableView.reloadData()
            } else {
                searchTextField.becomeFirstResponder()
                delegate?.searchBar(self, textEditEnded: searchTextField.text)
            }
        }
    }
    
    fileprivate var listForTable: [String] {
        get {
            if let f = filter {
                let filteredArray = commonList.filter{ $0.range(of: f, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
                return filteredArray
            }
            
            return []
        }
    }

    // MARK: - Interface Builder
    override open func prepareForInterfaceBuilder() {
        layer.backgroundColor = searchBarColor.cgColor
    }

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: searchBarNibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        addSubview(view)
    }

    // MARK: - Setup
    func setupCommonList(_ array: [String]) {
        commonList.removeAll()
        commonList.append(contentsOf: array)
        tableView.reloadData()
    }
    
    // MARK: - Actions
    @IBAction func rightButtonAction(_ sender: Any) {
        hasButtonDeleteStatus = !hasButtonDeleteStatus
    }
    
    // MARK: - Resize
    fileprivate func resizeTableView() {
        tableView.reloadData()
        
        switch listForTable.count {
        case 0:
            delegate?.searchBar(self, resized: heightViewMin)
            break
        case 1:
            delegate?.searchBar(self, resized: heightViewMiddle)
            break
        case 2,3:
            let h = CGFloat(55) + CGFloat(listForTable.count * 30)
            delegate?.searchBar(self, resized: h)
            break
        default:
            delegate?.searchBar(self, resized: heightViewMax)
        }
    }
}

// MARK: - UITextFieldDelegate
extension OBSearchBar: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        hasButtonDeleteStatus = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text {
            filter = text + string
            delegate?.searchBar(self, textInputed: filter)

            resizeTableView()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        hasButtonDeleteStatus = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        delegate?.searchBarKeyboardWasClosed(self)
        
        return true
    }
}

// MARK: - UITableViewDataSource
extension OBSearchBar: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listForTable.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: self.reusableCellIdentifier)
        cell.selectionStyle = .none
        cell.textLabel?.text = listForTable[indexPath.row]
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension OBSearchBar: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.searchBar(self, textEditEnded: listForTable[indexPath.row])
        filter = ""
        searchTextField.text = ""
        searchTextField.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRow
    }
}
